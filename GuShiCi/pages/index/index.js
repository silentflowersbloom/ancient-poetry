// index.js

const jinrishici = require('../../utils/jinrishici.js')

// wx.cloud.init({   traceUser: true,
//   env:'cloud1-1grn69gh9be4f1c3'})
const db = wx.cloud.database()
// 获取应用实例
const app = getApp()
var content,author
//var ciju,title,content,author
//ciju搜索框中输入的词句
//title返回诗词标题
//content返回诗词内容
//author搜返回作者
Page({
  data: {
    jinrishici:[],
    gushi:[],
    author1:"",
    
    
    isPlayingMusic:true,
    name:"",
    password:"",
    item: 0,  //swiper组件的current属性的默认值
    tab: 0,   //代表单击相应的标签，添加样式

    //诗词查询所需要的
    ciju:"",//搜索框中输入的词句
    title:"",//返回诗词标题
    content:"",//返回诗词内容
    author:"",//搜返回作者
    gu:"",//json对象

  },
  text3:"https://v1.jinrishici.com/rensheng.txt",
  bgm:null,
  onReady(){
    this.bgm=wx.getBackgroundAudioManager()
    this.bgm.title="忆夏思乡"    //音乐标题，必须写
    this.bgm.epname='忆夏思乡'   //专辑名称
    this.bgm.singer="MoreanP"    //演唱者
    this.bgm.coverImgUrl="https://s3.bmp.ovh/imgs/2022/01/c7f7cdd0e165190f.png", //专辑封面
    // this.bgm.onCanplay(()=> {
    //   this.bgm.pause()
    // })  
    // this.bgm.src='https://bjetxgzv.cdn.bspapp.com/VKCEYUGU-hello-uniapp/2cc220e0-c27a-11ea-9dfb-6da8e309e0d8.mp3',
    this.bgm.src='http://music.163.com/song/media/outer/url?id=506650377.mp3',
    this.bgm.onEnded(()=>{      //重新赋值时会自动播放
      this.bgm.src='http://music.163.com/song/media/outer/url?id=506650377.mp3'
    })
    this.bgm.onPause(()=>{
      this.setData({
        isPlayingMusic:false
      })
    })
    this.bgm.onPlay(()=>{
      this.setData({
        isPlayingMusic:true
      })
    })
  
  },
  play(){      //单击控制音乐的播放与暂停
    if(this.data.isPlayingMusic){
      this.bgm.pause()
    }
    else {
      this.bgm.play()
    }
    this.setData({
      isPlayingMusic:!this.data.isPlayingMusic
    })
  },
  
  // 滑动页面，标签相应切换
  changeItem:function(e) {
    this.setData({
      item: e.target.dataset.item,
      tab: e.target.dataset.item
    })
  },
  // 标签切换，下面内容相应切换
  changeTab: function(e) {
    this.setData({
      tab: e.detail.current
    })
  },




  //获取输入的内容
  inCiju:function(e){
    this.data.ciju = e.detail.value;
},
getGuInfo1:function (e) {
  var that=this;
  console.log(this.data.ciju);
  db.collection("poems").where({	 	//collectionName 表示欲模糊查询数据所在collection的名
  content: db.RegExp({
    regexp: this.data.ciju,
    options: 's'
  })
}).get({
  success: res=>{
    console.log(JSON.stringify(res.data))
    console.log(res.data)
 
    this.setData({
      gushi: JSON.stringify(res.data),
    })
    
     const shici = JSON.stringify(res.data)
    // var array=new Array()
    // var array=JSON.stringify(res.data)
    wx.navigateTo({
       url: '../search/search?shici='+shici,
       })
     }
  })

},

getGuInfo2:function (e) {
  var that=this;
  console.log(this.data.ciju);
 
  db.collection("poems").where({	 	//collectionName 表示欲模糊查询数据所在collection的名
  author: db.RegExp({
    regexp: this.data.ciju,
    options: 's'
  })
}).get({
  success: res=>{
    console.log(JSON.stringify(res.data))
    console.log(res.data)
 
    this.setData({
      gushi: JSON.stringify(res.data),
    })
    
     const shici = JSON.stringify(res.data)
    // var array=new Array()
    // var array=JSON.stringify(res.data)
    wx.navigateTo({
       url: '../search/search?shici='+shici,
       })
     }
  })

},
getGuInfo3:function (e) {
  var that=this;
  console.log(this.data.ciju);

   db.collection("poems").where({	 	//collectionName 表示欲模糊查询数据所在collection的名
  origin: db.RegExp({
    regexp: this.data.ciju,
    options: 's'
  })
}).get({
  success: res=>{
    console.log(JSON.stringify(res.data))
    console.log(res.data)
 
    this.setData({
      gushi: JSON.stringify(res.data),
    })
    
     const shici = JSON.stringify(res.data)
    // var array=new Array()
    // var array=JSON.stringify(res.data)
    wx.navigateTo({
       url: '../search/search?shici='+shici,
       })
     }
  })

},





onLoad: function (options) {
  var that = this
jinrishici.load(result => {
  // 下面是处理逻辑示例
  console.log(result)
  this.setData(
    {
      "jinrishici": result.data.content,
      "dynasty":result.data.origin.dynasty,
      "author":result.data.origin.author,
      "title":result.data.origin.title,

    }
    )
  
})


},
shuaxin:function () {
  wx.reLaunch({
    url: '../index/index',
  })
  
}

 


})
