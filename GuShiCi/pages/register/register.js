// pages/register/register.js
Page({

     /**
      * 页面的初始数据
      */
     data: {
          name:"",
          password:""

     },

      //获取name值
      name:function(e){
          this.data.name=e.detail.value;
          },
     
     //获取password值
     password:function(e){
      this.data.password=e.detail.value;
     },
       
     register(){
          if(this.data.name&&this.data.password){//不为空
               //向后端发送请求
            wx.request({
              url: 'http://localhost:8080/Poems/user/register',
              data:{
                   "name":this.data.name,
                   "password":this.data.password
              },
              success(res){
                if(res.data=="ok"){
                    wx.switchTab({
                      url: '../login/login',
                    })
                    console.log("注册成功");
                }else{
                 wx.showToast({
                   title: '用户已存在',
                   icon:"none",
                   duration:2000
                 })
                }
              }
            })
     
              
          }else{//为空
     
            wx.showToast({
              title: '用户名或密码不能为空',
              icon:"none",
              duration:2000
            })
     
          }

     }  
     
})