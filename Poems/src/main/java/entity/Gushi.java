package entity;

public class Gushi {
    private Integer id;//序号
    private Integer author_id;//作者序号
    private String title;//诗名
    private String content;//内容
    private String author;//作者


    public Gushi() {
    }

    public Gushi(String title, String content, String author) {
        this.title = title;
        this.content = content;
        this.author = author;

    }

    public Gushi(Integer id, Integer author_id, String title, String content, String author) {
        this.id = id;
        this.author_id = author_id;
        this.title = title;
        this.content = content;
        this.author = author;
    }
//键盘 alt+ins键自动建立
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAuthor_id() {
        return author_id;
    }

    public void setAuthor_id(Integer author_id) {
        this.author_id = author_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Gushi{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", author='" + author + '\''  +
                '}';
    }

    public static void main(String[] args) {

    }

    public void hello(){
        System.out.println("hello======");
    }

    public void hello(Integer id,String title){
        System.out.println(id+"===="+title);
    }

    public void world(){
        System.out.println("world======");
    }


}
