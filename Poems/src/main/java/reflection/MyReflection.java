package reflection;

import entity.Gushi;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


public class MyReflection {
    //面向对象
    //反射:动态编译 JVM运行时期可以获取类的信息

    //使用反射的步骤:
    //1.获取类信息==>Class
    public static void main(String[] args) {
        Class clazz=Gushi.class;

       //使用反射获取类中的所有字段
        Field[] fields=clazz.getDeclaredFields();//fields存储类中所有的字段信息
        for(Field field:fields){
            System.out.println(field.getName());//获取字段名称
        }

        try {
            Field field=clazz.getDeclaredField("title");
            //System.out.println(field.getName());
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        String name="hello";
        //clazz.newInstance()
        Gushi gushi=new Gushi();
        Method methods[]=clazz.getDeclaredMethods();//获取类中所有的方法(不包括构造方法)
        System.out.println("----------");
        for(Method method:methods){
            //System.out.println(method.getName());

//            if(method.getName().equals(name)){//当前方法名和name值相同
//                try {
//                    method.invoke(clazz.newInstance());//调用方法
//                } catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                } catch (InvocationTargetException e) {
//                    e.printStackTrace();
//                } catch (InstantiationException e) {
//                    e.printStackTrace();
//                }
//            }
        }


        try {
            Method method= clazz.getDeclaredMethod(name,Integer.class,String.class);
            method.invoke(clazz.newInstance(),20,"望庐山瀑布");//传参

            //通过获取类的构造方法创建一个对象
            Gushi gushi1= (Gushi) clazz.getConstructor(String.class,String.class,String.class).
                    newInstance("望庐山瀑布","日照香炉生紫烟，遥看瀑布挂前川。飞流直下三千尺，疑是银河落九天。","李白");

            System.out.println(gushi1);




        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }


    }




}
