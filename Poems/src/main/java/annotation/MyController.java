package annotation;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@RequestMapping("/stu")
public class MyController {

    @RequestMapping("/getStuInfo")
    public void select(){
        System.out.println("查询学生信息");
    }

    @RequestMapping("/update")
    public void update() {
        System.out.println("更新学生信息");
    }
    @RequestMapping("/delete")
    public void delete() {
        System.out.println("删除学生信息");
    }


    //
    public static void main(String[] args) {
        String url="/delete";//前端发送的请求路径
        Class clazz=MyController.class;
        if(clazz.isAnnotationPresent(RequestMapping.class)){//判断当前类上是否有RequestMapping注解
            Method methods[]=clazz.getDeclaredMethods();//获取类中所有的方法
            for(Method method:methods){
                if(method.isAnnotationPresent(RequestMapping.class)){//判断当前方法是否有RequestMapping注解
                    RequestMapping requestMapping=method.getAnnotation(RequestMapping.class);//获取注解对象,目的是为了获取注解值
                      if(requestMapping.value().equals(url)){//全部匹配,找到请求对应的方法
                          try {
                              method.invoke(clazz.newInstance());//调用方法
                          } catch (IllegalAccessException e) {
                              e.printStackTrace();
                          } catch (InvocationTargetException e) {
                              e.printStackTrace();
                          } catch (InstantiationException e) {
                              e.printStackTrace();
                          }
                      }
                }
            }
        }
    }

}
