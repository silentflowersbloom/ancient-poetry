package annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE,ElementType.METHOD})//当前注解可以用在类,接口,枚举或方法上
@Retention(RetentionPolicy.RUNTIME) //JVM运行时期可以通过反射获取注解值
public @interface RequestMapping {//一个注解
    String value() default "good";//是一个字段值
}
