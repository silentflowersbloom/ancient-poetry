package servlet;

// 登录和注册服务
import entity.User;
import util.DBUtil;
import util.MD5Util;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@WebServlet("/user/*")
public class LoginAndRegisterServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String url=req.getRequestURI();//获取完整请求名
        url=url.substring(url.lastIndexOf("/")+1);
        System.out.println(url);

        //1.获取当前类信息
        Class clazz=this.getClass();

        try {
            //2.通过url找到当前类的对应的方法
            Method method=clazz.getDeclaredMethod(url,HttpServletRequest.class,HttpServletResponse.class);
            method.invoke(clazz.newInstance(),req,resp);//调用当前方法

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }



    public void login(HttpServletRequest req, HttpServletResponse resp){
        String name=req.getParameter("name");
        String password=req.getParameter("password");
        System.out.println(name+","+password);
        //接受到参数后,应查询当前用户名是否存在,存在=>进一步比较密码  不存在=>提示当前用户不存在
        String sql="select * from user where username=?";
        User user=DBUtil.getObject(sql, User.class,name);
        System.out.println(user);


        if(user!=null){
            if(MD5Util.checkPassword(password,user.getPassword())){
                try {
                    resp.getWriter().write("ok");//用户名和密码都匹配成功
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }else{//用户名正确,密码错误
                try {
                    resp.getWriter().write("no");
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }

        }else{//用户名不存在
            try {
                resp.getWriter().write("notExist");
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }


    public void register(HttpServletRequest req, HttpServletResponse resp){
        String name=req.getParameter("name");
        String password=req.getParameter("password");
        String sql="select * from user where username=?";
        User user=DBUtil.getObject(sql, User.class,name);//先查询用户名是否存在,存在不允许注册
        System.out.println(user);
        if(user == null){//用户已存在,不允许注册
//            try {
//                resp.getWriter().write("exist");
//            } catch (IOException ioException) {
//                ioException.printStackTrace();
//            }
                        User user1=new User(name,MD5Util.MD5PassWord(password));
            String sql2="insert into user(username,password) values(?,?)";
            if(DBUtil.insertObject(sql2,user1)>0){
                try {
                    resp.getWriter().write("ok");
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        }else{
            try {
                resp.getWriter().write("exist");
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
//            //先加密密码再存入
//            User user1=new User(name,MD5Util.MD5PassWord(password));
//            String sql2="insert into user(username,password) values(?,?)";
//            if(DBUtil.insertObject(sql2,user1)>0){
//                try {
//                    resp.getWriter().write("ok");
//                } catch (IOException ioException) {
//                    ioException.printStackTrace();
//                }
//            }
        }
        System.out.println("我是register");
    }
}
