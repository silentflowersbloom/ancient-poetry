package servlet;

import com.alibaba.fastjson.JSON;
import entity.Gushi;
import util.DBUtil;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

@WebServlet("/gushi/*")

public class GushiServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String url=req.getRequestURI();//获取完整请求名
        url=url.substring(url.lastIndexOf("/")+1);
        System.out.println(url);

        //1.获取当前类信息
        Class clazz=this.getClass();

        try {
            //2.通过url找到当前类的对应的方法
            Method method=clazz.getDeclaredMethod(url,HttpServletRequest.class,HttpServletResponse.class);
            method.invoke(clazz.newInstance(),req,resp);//调用当前方法

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }


    public  void getGuInfo1(HttpServletRequest req, HttpServletResponse resp){
        String ciju=req.getParameter("ciju");
        System.out.println(ciju);
        String sql="select title, content, author from poems where content like '%?%'";
        System.out.println(sql);
        List<Gushi> gushi= DBUtil.getList(sql, Gushi.class,ciju);
        System.out.println(gushi);
        // fastjson
        System.out.println("json字符串===>"+JSON.toJSON(gushi));
        //JSON.parseObject(JSON.toJSON(student).toString());把json对象字符串转成json对象

        try {
            //往前端回一个json对象
            resp.getWriter().println(JSON.parseObject(JSON.toJSON(gushi).toString()));
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
 /*     //json对象(json字符串):
        {"id":1,"name":"tom","gender":"男","classes":"1班","phone":{"num":"123","addr":"cz"}}
        //json数组
        [
        {"id":1,"name":"tom","gender":"男","classes":"1班","phone":"123"},
        {"id":2,"name":"tom","gender":"男","classes":"1班","phone":"456"},
        {"id":3,"name":"tom","gender":"男","classes":"1班","phone":"789"}
        ]
 * */
    }


    public  void getGuInfo2(HttpServletRequest req, HttpServletResponse resp){
        String ciju=req.getParameter("ciju");
        System.out.println(ciju);
        String sql="select title, content from poems where author=?";
        System.out.println(sql);
        Gushi gushi= DBUtil.getObject(sql, Gushi.class,ciju);
        System.out.println(gushi);
        // fastjson
        System.out.println("json字符串===>"+JSON.toJSON(gushi));
        //JSON.parseObject(JSON.toJSON(student).toString());把json对象字符串转成json对象, author

        try {
            //往前端回一个json对象
            resp.getWriter().println(JSON.parseObject(JSON.toJSON(gushi).toString()));
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
 /*     //json对象(json字符串):
        {"id":1,"name":"tom","gender":"男","classes":"1班","phone":{"num":"123","addr":"cz"}}
        //json数组
        [
        {"id":1,"name":"tom","gender":"男","classes":"1班","phone":"123"},
        {"id":2,"name":"tom","gender":"男","classes":"1班","phone":"456"},
        {"id":3,"name":"tom","gender":"男","classes":"1班","phone":"789"}
        ]
 * */
    }


    public  void getGuInfo3(HttpServletRequest req, HttpServletResponse resp){
        String ciju=req.getParameter("ciju");
        System.out.println(ciju);
        String sql="select title, content, author from poems where title=?";
        System.out.println(sql);
        Gushi gushi= DBUtil.getObject(sql, Gushi.class,ciju);
        System.out.println(gushi);
        // fastjson
        System.out.println("json字符串===>"+JSON.toJSON(gushi));
        //JSON.parseObject(JSON.toJSON(student).toString());把json对象字符串转成json对象

        try {
            //往前端回一个json对象
            resp.getWriter().println(JSON.parseObject(JSON.toJSON(gushi).toString()));
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
 /*     //json对象(json字符串):
        {"id":1,"name":"tom","gender":"男","classes":"1班","phone":{"num":"123","addr":"cz"}}
        //json数组
        [
        {"id":1,"name":"tom","gender":"男","classes":"1班","phone":"123"},
        {"id":2,"name":"tom","gender":"男","classes":"1班","phone":"456"},
        {"id":3,"name":"tom","gender":"男","classes":"1班","phone":"789"}
        ]
 * */
    }


    public void modifyGu(HttpServletRequest req, HttpServletResponse resp){
        Gushi gushi=new Gushi();
        gushi.setAuthor_id(Integer.parseInt(req.getParameter("author_id")));
        gushi.setTitle(req.getParameter("title"));
        gushi.setContent(req.getParameter("content"));
        gushi.setAuthor(req.getParameter("author"));


        String sql="update student set name=?,gender=?,classes=?,phone=? where id=?";
        DBUtil.updateObject(sql,gushi);
        try {
            resp.getWriter().write("ok");
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }


    }

}
