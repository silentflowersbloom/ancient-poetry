    <%
    String cont = new String(request.getParameter("word").getBytes("ISO-8859-1"),"UTF-8");
    System.out.println(cont);
	Connection conn = null;
	PreparedStatement pstmt = null;


	try{
		//1.加载数据库
		Class.forName("com.mysql.cj.jdbc.Driver");

		//2.连接数据库
		String url = "jdbc:mysql://localhost:3306/poetry1?serverTimezone=UTC&characterEncoding=utf-8";
		String user = "root";
		String password = "123456";
		conn = DriverManager.getConnection(url,user,password);
		System.out.println("数据库连接成功！");
		Statement stmt = null;
		ResultSet rs = null;


		String sql = "SELECT `title`,`content`,`author` FROM poemscollect where `content` like '%"+cont+"%'"; //查询语句
		stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);//conn.createStatement();
		rs = stmt.executeQuery(sql);


		 int pageSize;//一页显示的记录数
		   int totalItem;//记录总数
		   int totalPage;//总页数
		   int curPage;//待显示页码
		   String strPage;
		   int i;
		   pageSize=10;//设置一页显示的记录数
		   strPage=request.getParameter("page");//获得待显示页码
		   if(strPage==null)
		   {
		    curPage=1;
		   }
		   else
		   {
		    curPage=java.lang.Integer.parseInt(strPage);//将字符串转换成整形
		   }
		   if(curPage<1)
		   {
		    curPage=1;
		   }
		   rs.last();//获取记录总数

		   totalItem=rs.getRow();
		   totalPage=(totalItem+pageSize-1)/pageSize;
		   if(curPage>totalPage) curPage=totalPage;//调整待显示的页码
		   if(totalPage>0)
		   {//将记录指针到待显示页的第一条记录上
		    rs.absolute((curPage-1)*pageSize+1);
		   }
		   i=0;


	%>

				  <%//显示数据
       while(i<pageSize && !rs.isAfterLast())
       {
       %>

    			<tr>
    				<td>
    				<%=rs.getString("title")%>
    				</td>
    				<td><%=rs.getString("content")%></td>
    				<td style="text-align: center;  "><%=rs.getString("author")%></td>
    			</tr>
    			  <%
       rs.next();
       i++;
       }
       rs.close();
       stmt.close();
       conn.close();
       %>
    			</table>
    			</div>
    			<div class="fanye">
    			<p >

       		共<%=totalItem%>个记录,分<%=totalPage%>页显示,当前页是:第<%=curPage%>页

       		<%if(curPage>1){%><a href="collection.jsp?page=1">首页</a><%}%>

       		<%if(curPage>1){%><a href="collection.jsp?page=<%=curPage-1%>">上一页</a><%}%>

       	<%
       	//for(int j=1;j<=totalPage;j++)
       	//{
        //	out.print("  <a href='page.jsp?page="+j+"'>"+j+"</a>");
       //}
       %>


       <%if(curPage<totalPage){%><a href="collection.jsp?page=<%=curPage+1%>">下一页</a><%}%>

       <%if(totalPage>1){%><a href="collection.jsp?page=<%=totalPage%>">末页</a><%}%>
       </p>

    <%
    }
    catch(SQLException e1){out.println(e1);}
    %>
